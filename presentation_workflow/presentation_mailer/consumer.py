import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_rejection(dictionary):
    send_mail(
        "Your presentation has been rejected",
        f"{dictionary['name']}, we're sorry to tell you that your presentation {dictionary['title']} has been rejected",
        "eample@email.com",
        [dictionary["email"]],
        fail_silently=False,
    )


def process_approval(dictionary):
    send_mail(
        "Your presentation has been approved",
        f"{dictionary['name']}, we're happy to tell you that your presentation {dictionary['title']} has been approved",
        "eample@email.com",
        [dictionary["email"]],
        fail_silently=False,
    )


def process_message(ch, method, properties, body):
    dictionary = json.loads(body)
    status = dictionary["status"]
    if status == "approved":
        process_approval(dictionary)

    if status == "rejected":
        process_rejection(dictionary)


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="tasks")
channel.basic_consume(
    queue="tasks",
    on_message_callback=process_message,
    auto_ack=True,
)
channel.start_consuming()

# import json
# import pika
# import django
# import os
# import sys
# import time
# from django.core.mail import send_mail
# from pika.exceptions import AMQPConnectionError

# sys.path.append("")
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
# django.setup()

# parameters = pika.ConnectionParameters(host="rabbitmq")
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# while True:
#     try:

#         def process_approval(ch, method, properties, body):
#             body = json.loads(body)
#             print(body)
#             name = body["presenter_name"]
#             title = body["title"]

#             send_mail(
#                 "Your presentation has been accepted",
#                 f"{name} we are happy to tell you that {title} has been accepted.",
#                 "admin@conference.go",
#                 [body["presenter_mail"]],
#                 fail_silently=False,
#             )

#         channel.queue_declare(queue="presentation_approvals")
#         channel.basic_consume(
#             queue="presentation_approvals",
#             on_message_callback=process_approval,
#             auto_ack=True,
#         )

#         def process_rejection(ch, method, properties, body):
#             print("this is working")
#             body = json.loads(body)
#             name = body["presenter_name"]
#             title = body["title"]

#             send_mail(
#                 "Your presentation has been Denied",
#                 f"{name} we are sorry to inform you {title} has been denied.",
#                 "admin@conference.go",
#                 [body["presenter_mail"]],
#                 fail_silently=False,
#             )

#         channel.queue_declare(queue="presentation_rejections")
#         channel.basic_consume(
#             queue="presentation_rejections",
#             on_message_callback=process_rejection,
#             auto_ack=True,
#         )
#         channel.start_consuming()
#     except AMQPConnectionError:
#         print("Could not connect to RabbitMQ")
#         time.sleep(2.0)
