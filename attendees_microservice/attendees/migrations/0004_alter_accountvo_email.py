# Generated by Django 4.0.3 on 2023-05-17 22:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0003_accountvo_email_accountvo_first_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountvo',
            name='email',
            field=models.EmailField(max_length=254, null=True),
        ),
    ]
