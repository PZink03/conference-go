from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference
from events.api_views import ConferenceListEncoder
import pika
from .producer import send_message


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]

    def get_extra_data(self, o):
        return {"status": o.status}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.all()

        return JsonResponse(
            {"presentations": presentations}, encoder=PresentationDetailEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            presentation = Presentation.objects.get(id=id)
            # content["attendees"] = attendee
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Presentation id"},
                status=400,
            )
    Presentation.objects.update(**content)
    presentation = Presentation.objects.get(id=id)

    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


# @require_http_methods(["PUT"])
# def api_approve_presentation(request, id):
#     presentation = Presentation.objects.get(id=id)
#     presentation.approve()
#     parameters = pika.ConnectionParameters(host="rabbitmq")
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue="presentation_approvals")
#     channel.basic_publish(
#         exchange="",
#         routing_key="presentation_approvals",
#         body=json.dumps(
#             {
#                 "presenter_name": presentation.presenter_name,
#                 "presenter_mail": presentation.presenter_email,
#                 "title": presentation.title,
#             }
#         ),
#     )
#     connection.close()
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )


# @require_http_methods(["PUT"])
# def api_reject_presentation(request, id):
#     presentation = Presentation.objects.get(id=id)
#     presentation.reject()
#     parameters = pika.ConnectionParameters(host="rabbitmq")
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue="presentation_rejections")
#     channel.basic_publish(
#         exchange="",
#         routing_key="presentation_rejections",
#         body=json.dumps(
#             {
#                 "presenter_name": presentation.presenter_name,
#                 "presenter_mail": presentation.presenter_email,
#                 "title": presentation.title,
#             }
#         ),
#     )
#     connection.close()
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )
@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()
    send_message(presentation, "approved")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    send_message(presentation, "rejected")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
